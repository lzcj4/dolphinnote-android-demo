package com.tannuo.dolphinnotedemo;

import com.tannuo.dolphinnotedemo.sdk.util.DataUtil;

import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    /**
     * 从高位到低位，放入byteBuffer后取出来会得到正确的结果
     */
    @Test
    public void int2byte() throws Exception {
        int source = new Random().nextInt();
        byte[] bytes = DataUtil.int2byteBigEndian(source);
        ByteBuffer byteBuffer = ByteBuffer.allocate(bytes.length);
        byteBuffer.put(bytes);
        byteBuffer.rewind();
        int result = byteBuffer.getInt();
        assertEquals(source, result);
    }

    @Test
    public void byte2int() throws Exception {
        int source = new Random().nextInt();
        byte[] bytes = DataUtil.int2byteLittleEndian(source);
        int result = DataUtil.bytesToIntLittleEndian(bytes);
        assertEquals(source, result);
    }
}