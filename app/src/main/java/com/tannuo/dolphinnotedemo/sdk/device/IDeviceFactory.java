package com.tannuo.dolphinnotedemo.sdk.device;

import android.content.Context;

import com.tannuo.dolphinnotedemo.sdk.device.protocol.IProtocol;

/**
 * Created by nick on 2016/4/23.
 */
public interface IDeviceFactory {
    /**
     * getFactory bluetooth connect service
     *
     * @param context
     * @param protocol
     * @param vendorId
     * @return
     */
    IDevice get(Context context, IProtocol protocol, int vendorId);
}
