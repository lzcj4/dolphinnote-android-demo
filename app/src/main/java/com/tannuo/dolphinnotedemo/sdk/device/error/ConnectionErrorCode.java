package com.tannuo.dolphinnotedemo.sdk.device.error;

/**
 * Created by hucn on 2016/8/2.
 * Description: 连接错误的返回类
 */
public class ConnectionErrorCode {

    public static final int WIFI_ERROR_NONE = 0;
    public static final int WIFI_ERROR_CONN_FAILED = 1;
    public static final int WIFI_ERROR_CONN_LOST = 2;
    public static final int WIFI_ERROR_DEV_NOT_FOUND = 3;
    public static final int WIFI_ERROR_NOT_ENABLE = 4;
    public static final int WIFI_ERROR_INIT_FAILED = 5;
    public static final int WIFI_ERROR_MULTI_FAILED = 6;

    public int ErrCode;
    public String Title;
    public String Msg;

    public ConnectionErrorCode(int errCode, String msg) {
        this(errCode, msg, "title");
    }

    public ConnectionErrorCode(int errCode, String title, String msg) {
        ErrCode = errCode;
        Title = title;
        Msg = msg;
    }
}
