package com.tannuo.dolphinnotedemo.sdk.device.wifi;

import android.content.Context;

import com.tannuo.dolphinnotedemo.sdk.device.DeviceCanvas;
import com.tannuo.dolphinnotedemo.sdk.device.IDevice;
import com.tannuo.dolphinnotedemo.sdk.device.DeviceScreen;
import com.tannuo.dolphinnotedemo.sdk.device.protocol.IProtocol;
import com.tannuo.dolphinnotedemo.sdk.device.protocol.ProtocolHandler;

/**
 * Created by hucn on 2016/7/29.
 * Description:
 */
public abstract class WifiDeviceBase implements IDevice {

    // Constants that indicate the current connection state
    protected static final int WIFI_STATE_NONE = 0;       // we're doing nothing
    protected static final int WIFI_STATE_LISTEN = 1;     // now listening for incoming connections
    protected static final int WIFI_STATE_CONNECTING = 2; // now initiating an outgoing connection
    protected static final int WIFI_STATE_CONNECTED = 3;  // now startIO to a remote device
    protected static final int WIFI_STATE_READY = 4;  // now startIO to a remote device
    protected static final int WIFI_STATE_DISCONNECTED = 5;  // now startIO to a remote device
    protected static final int WIFI_STATE_SEARCHING = 6;  // now startIO to a remote device
    protected static final int WIFI_STATE_WIFI_ERROR = 7;  // now wifi is not enable
    protected static final int WIFI_STATE_WIFI_INIT = 8;  // now wifi is not enable
    protected static final int WIFI_STATE_WIFI_NOT_ENABLE = -1;  // now wifi is not enable

/*    public static final int WIFI_ERROR_NONE = 0;
    public static final int WIFI_ERROR_CONN_FAILED = 1;
    public static final int WIFI_ERROR_CONN_LOST = 2;
    public static final int WIFI_ERROR_DEV_NOT_FOUND = 3;
    public static final int WIFI_ERROR_NOT_ENABLE = 4;
    public static final int WIFI_ERROR_INIT_FAILED = 5;
    public static final int WIFI_ERROR_MULTI_FAILED = 6;*/

    protected String mDeviceName;

    protected Context mContext;
    protected WifiService mWifiService;
    protected DeviceCanvas mDeviceCanvas;
    protected IProtocol mProtocol;
    protected DeviceScreen mDeviceScreen;
    protected ProtocolHandler mProtocolHandler;

}
