package com.tannuo.dolphinnotedemo.sdk.util;

/**
 * Created by nick on 2016/4/23.
 */
public class DataUtil {
    public static int byteToUnsignedByte(byte b) {
        int result = b & 0xFF;
        return result;
    }

    /**
     * @param bytes
     * @return
     */
    public static int bytesToIntLittleEndian(byte... bytes) {
        if (null == bytes || bytes.length == 0) {
            return 0;
        }
        if (!(bytes.length == 4 || bytes.length == 2)) {
            throw new IllegalArgumentException("invalid bytes'len");
        }

        int len = bytes.length - 1;
        int result = 0;
        for (int i = len; i >= 0; i--) {
            int b = byteToUnsignedByte(bytes[i]);
            result += b << 8 * i;
        }
        return result;
    }

    /**
     * int 转换为数组
     */
    public static byte[] int2byteBigEndian(int res) {
        byte[] targets = new byte[4];
        targets[3] = (byte) (res & 0xff);// 最低位
        targets[2] = (byte) ((res >> 8) & 0xff);// 次低位
        targets[1] = (byte) ((res >> 16) & 0xff);// 次高位
        targets[0] = (byte) (res >>> 24);// 最高位,无符号右移。
        return targets;
    }

    /**
     * int 转换为数组
     */
    public static byte[] int2byteLittleEndian(int res) {
        byte[] targets = new byte[4];
        targets[0] = (byte) (res & 0xff);// 最低位
        targets[1] = (byte) ((res >> 8) & 0xff);// 次低位
        targets[2] = (byte) ((res >> 16) & 0xff);// 次高位
        targets[3] = (byte) (res >>> 24);// 最高位,无符号右移。
        return targets;
    }

    /**
     * int 转换为程度为 2 的 byte[]数组,BigEndian
     */
    public static byte[] int2byteLen2Low(int res) {
        byte[] bytes = int2byteBigEndian(res);
        return new byte[]{bytes[3], bytes[2]};
    }

    /**
     * int 转换为程度为 3 的 byte[]数组,BigEndian
     */
    public static byte[] int2byteLen3Low(int res) {
        byte[] bytes = int2byteBigEndian(res);
        return new byte[]{bytes[3], bytes[2], bytes[1]};
    }

    /**
     * long 转byte[]数组
     */
    public static byte[] longToByte(long number) {
        byte[] targets = new byte[8];
        targets[0] = (byte) (number & 0xff);// 最低位
        targets[1] = (byte) ((number >> 8) & 0xff);// 次低位
        targets[2] = (byte) ((number >> 16) & 0xff);// 次高位
        targets[3] = (byte) ((number >> 24) & 0xff);// 最高位,无符号右移。
        targets[4] = (byte) ((number >> 32) & 0xff);// 最低位
        targets[5] = (byte) ((number >> 40) & 0xff);// 次低位
        targets[6] = (byte) ((number >> 48) & 0xff);// 次高位
        targets[7] = (byte) (number >>> 56);// 最高位,无符号右移。
        return targets;
    }
}
