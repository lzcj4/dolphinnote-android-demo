package com.tannuo.dolphinnotedemo.sdk.device;

/**
 * Created by hucn on 2016/8/2.
 * Description:
 */
public interface MessageReceiveListener {

    void onReceive(byte[] data);
    
}
