package com.tannuo.dolphinnotedemo.sdk.device;

/**
 * Created by hucn on 2016/7/29.
 * Description:
 */
public interface CommandListener {

    void onError(int errorCode);

    void onCommand(int commandCode, Object obj);

}
