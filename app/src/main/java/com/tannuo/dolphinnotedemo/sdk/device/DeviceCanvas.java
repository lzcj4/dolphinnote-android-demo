package com.tannuo.dolphinnotedemo.sdk.device;

import android.util.SparseArray;

import com.tannuo.dolphinnotedemo.sdk.device.protocol.IMessage;
import com.tannuo.dolphinnotedemo.sdk.util.DataUtil;
import com.tannuo.dolphinnotedemo.sdk.util.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hucn on 2016/7/28.
 * Description: 一个虚拟的设备的画布的映射
 */
public class DeviceCanvas {

    private final String TAG = DeviceCanvas.class.getSimpleName();

    public static final int PAINTMODE_DOT_1 = 0;
    public static final int PAINTMODE_DOT_2 = 1;
    public static final int PAINTMODE_LINE_1 = 2;
    public static final int PAINTMODE_LINE_2 = 3;

    public static final int PATH_BUF_MAX_LEN = 100;

    // TODO: 2016/8/2 点的长度还未决定！！！！！！！！！！！！！！！！！
    private final int elementLength = 12;

    // 屏幕的属性
    public int mPaintMode = PAINTMODE_DOT_1;
    public int mGesture;
    public int mNumOfPoints;
    public int mSnapshot;

    private int mRedMin;
    private int mRedMax;

    // 缓存类
    private IPointsCache mCache;

    private TouchPointListener mTouchPointListener;


    public DeviceCanvas(int redMin, int redMax, TouchPointListener touchPointListener) {
        mRedMin = redMin;
        mRedMax = redMax;
        mTouchPointListener = touchPointListener;
        mCache = new SimpleCache();
        //// TODO: 2016/8/3 设置缓存的目录
    }

    public void reset() {
        mPaintMode = PAINTMODE_DOT_1;
        //mPoints.clear();
    }

    public void decodePoint(int pointLen, byte[] buffer) {
        if (pointLen <= 0 || buffer == null || (buffer.length % elementLength) != 0) {
            throw new IllegalArgumentException("point length or data invalid");
        }

        List<TouchPoint> mPoints = new ArrayList<>();

        // 保存到缓存中
        mCache.putData(buffer);

        SparseArray<TouchPoint> downMap = new SparseArray<>();
        SparseArray<TouchPoint> upMap = new SparseArray<>();

        for (int i = 0; i < pointLen; i++) {
            int pos = i * elementLength;
            TouchPoint point = new TouchPoint();

            // TODO: 2016/8/2 点的协议还未确定！！！！！！！！！！！！！！！！！
            point.setX(DataUtil.bytesToIntLittleEndian(buffer[pos + 2], buffer[pos + 3]));
            point.setY(DataUtil.bytesToIntLittleEndian(buffer[pos + 4], buffer[pos + 5]));
            point.setWidth(DataUtil.bytesToIntLittleEndian(buffer[pos + 6], buffer[pos + 7]));
            point.setHeight(DataUtil.bytesToIntLittleEndian(buffer[pos + 8], buffer[pos + 9]));
            point.setActionByDevice(buffer[pos]);
            point.setId(buffer[pos + 1]);

            float area = point.getArea();
            if ((area > mRedMin) && (area < mRedMax)) {
                point.setColor(TouchPoint.COLOR_RED);
            } else if (area >= mRedMax) {
                point.setColor(TouchPoint.COLOR_WHITE);
            } else {
                point.setColor(TouchPoint.COLOR_BLACK);
            }

            Logger.i(TAG, point.toString());
            if (point.getIsDown()) {
                downMap.put(point.getId(), point);
            } else {
                upMap.put(point.getId(), point);
            }
        }

        if (downMap.size() > 0) {
            for (int i = 0; i < downMap.size(); i++) {
                mPoints.add(downMap.valueAt(i));
            }
        }

        if (upMap.size() > 0) {
            for (int i = 0; i < upMap.size(); i++) {
                mPoints.add(upMap.valueAt(i));
            }
        }

        // 通知上层绘制
        if (mTouchPointListener != null) {
            if (!mPoints.isEmpty()) {
                TouchEvent event = new TouchEvent();
                event.setAction(TouchEvent.ACTION_TOUCH);
                event.setPoints(mPoints);
                mTouchPointListener.onTouchEvent(event);
            }
        }
    }

    //
    public void setPaintMode(int drawMode) {
        if (drawMode == PAINTMODE_DOT_1)
            mPaintMode = PAINTMODE_DOT_1;
        else if (drawMode == PAINTMODE_LINE_1)
            mPaintMode = PAINTMODE_LINE_1;
    }

    //
    public int getPaintMode() {
        return mPaintMode;
    }

    public void setNumOfPoints(int pointNum) {
        mNumOfPoints = pointNum;
    }

    public void setGesture(int newGesture) {
        mGesture = newGesture;
    }

    public void setSnapshot(int shot) {
        mSnapshot = shot;
    }

    /**
     * 保存当前画板的快照
     */
    public void snapshot() {
        // TODO: 2016/7/29 to save the current canvas points
        mCache.snapshot();
    }

    /**
     * 录制开始
     */
    public void startRecording() {
        // TODO: 2016/7/29 start to record
    }

    /**
     * 录制结束
     */
    public void stopRecording() {
        // TODO: 2016/7/29 stop to record

    }

    /**
     * 接收到点的数据，进行解析，然后通知上层绘制
     *
     * @param message 消息
     */
    public void setPoints(IMessage message) {
        decodePoint(message.getData().length, message.getData());
    }

    private void setOnTouchPointListener(TouchPointListener touchPointListener) {
        mTouchPointListener = touchPointListener;
    }

    public void setSavedDirectory(File file) {
        if (mCache != null) {
            mCache.setCacheFile(file);
        }
    }
}