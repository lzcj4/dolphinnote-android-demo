package com.tannuo.dolphinnotedemo.sdk.device;


import com.tannuo.dolphinnotedemo.sdk.device.protocol.IMessage;
import com.tannuo.dolphinnotedemo.sdk.util.DataUtil;

/**
 * Created by nick on 2016/04/22.
 * 屏幕类
 */

public class DeviceScreen {

    private final String TAG = DeviceScreen.class.getSimpleName();

    private IrTouch mIrTouch;
    public long mTouchScreenID;
    private float mScreenHeight;
    private float mScreenWidth;

    private String DeviceID;

    public void setIrTouchFeature(byte[] dataBuffer) {
        if (null == dataBuffer || dataBuffer.length != 9) {
            throw new IllegalArgumentException("Invalid data");
        }
        mIrTouch = new IrTouch();
        mIrTouch.mScreenXLED = DataUtil.bytesToIntLittleEndian(dataBuffer[0], dataBuffer[1]);
        mIrTouch.mScreenYLED = DataUtil.bytesToIntLittleEndian(dataBuffer[2], dataBuffer[3]);
        mIrTouch.mScreenLedInsert = dataBuffer[4];
        mIrTouch.mScreenLedDistance = DataUtil.bytesToIntLittleEndian(dataBuffer[5], dataBuffer[6]);
        mIrTouch.mScreenMaxPoint = dataBuffer[7];
        mIrTouch.mScreenFrameRate = dataBuffer[8];
    }

    public void setScreenWidth(float width) {
        mScreenWidth = width;
    }

    public float getScreenWidth() {
        return mScreenWidth;
    }

    void setScreenHeight(float height) {
        mScreenHeight = height;
    }

    public float getScreenHeight() {
        return mScreenHeight;
    }

    public void setID(long newID) {
        mTouchScreenID = newID;
    }

    public void reset() {
        mIrTouch = null;
        mScreenHeight = 0;
        mScreenWidth = 0;
        mTouchScreenID = 0;
    }

    public class IrTouch {
        public int mScreenXLED;
        public int mScreenYLED;
        public int mScreenLedInsert;
        public int mScreenLedDistance;
        public int mScreenMaxPoint;
        public int mScreenFrameRate;
    }


    /**
     * 设置屏幕信息
     *
     * @param message 点的信息
     */
    public void setDeviceInfo(IMessage message) {

    }

    public String getDeviceID() {
        return DeviceID;
    }

    public void setDeviceID(String deviceID) {
        DeviceID = deviceID;
    }
}
