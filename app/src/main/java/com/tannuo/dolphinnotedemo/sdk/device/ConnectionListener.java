package com.tannuo.dolphinnotedemo.sdk.device;


import com.tannuo.dolphinnotedemo.sdk.device.error.ConnectionErrorCode;

/**
 * Created by Nick_PC on 2016/6/20.
 * Description：连接状态监听类
 */
public interface ConnectionListener {

    /**
     * Disconnected or invalid operations
     *
     * @param errorCode 返回错误码
     */
    void onError(ConnectionErrorCode errorCode);

    /**
     * 初次连接不成功的返回，目前暂定为超过一定时间没有收到白板的返回
     */
    void onConnectFail();

    /**
     * 初次连接成功的返回
     */
    void onConnected();

    /**
     * 断开连接的返回
     */
    void onDisconnected();

    /**
     * 广播信息返回成功
     */
    void onMultiSuccess();

    /**
     * 广播信息超过一定时间未返回
     */
    void onMultiFail();
}
