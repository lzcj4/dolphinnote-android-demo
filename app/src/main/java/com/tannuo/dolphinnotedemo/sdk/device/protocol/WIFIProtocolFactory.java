package com.tannuo.dolphinnotedemo.sdk.device.protocol;

import java.security.InvalidParameterException;

/**
 * Created by Nick_PC on 2016/7/1.
 */
public class WIFIProtocolFactory implements IProtocolFactory {

    @Override
    public IProtocol getProtocol(int type) {
        IProtocol result = null;
        switch (type) {
            case ProtocolType.JY:
                // result = new JYProtocol(new JYTouchScreen(600, 2000));
                break;
            case ProtocolType.PQ:
                break;
            case ProtocolType.CVT:
                // result = new CVTProtocol();
                break;
            case ProtocolType.ZD:
                result = new ZDProtocol();
                break;
            default:
                throw new InvalidParameterException();
        }
        return result;
    }
}
