package com.tannuo.dolphinnotedemo.sdk.device.protocol;

/**
 * Created by hucn on 2016/7/28.
 * Description: 一个解析结果的接口
 */
public interface IMessage {
    byte[] getId();

    void setId(byte[] newValue);

    byte[] getData();

    void setData(byte[] newValue);

    byte[]  getFeature();

    void setFeature(  byte[]  newValue);

    void setFeature(  int  newValue);


    byte[] getBytes();


    int getFeatureInt();
}

