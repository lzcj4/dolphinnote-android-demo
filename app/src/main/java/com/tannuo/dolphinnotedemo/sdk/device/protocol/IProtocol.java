package com.tannuo.dolphinnotedemo.sdk.device.protocol;

/**
 * Created by nick on 2016/4/23.
 * 编解码协议的策略接口
 */

public interface IProtocol {

    IMessage decode(byte[] data);

    byte[] encode(IMessage message);

}
