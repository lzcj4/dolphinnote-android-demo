package com.tannuo.dolphinnotedemo.sdk.device.protocol;

/**
 * Created by Nick_PC on 2016/8/8.
 */
public interface ProtocolCode {

    //ACTION_ 不带返回指令
    //FUNC_ 有返回指令

    // 不带参数的消息类型
    /***
     * 由白板->手机，提示手机抓屏
     */
    byte ACTION_REQUEST_SNAPSHOT = (byte) 0xA0;
    /***
     * 白板<->手机，手机和白板连接相互结束，白板处待连状态
     */
    byte ACTION_REQUEST_TO_DISCONNECT = (byte) 0xA1;
    /***
     * 白板<->手机，请求对方再次发送上次的数据包
     */
    byte ACTION_REQUEST_TO_REPEAT = (byte) 0xA2;
    /***
     * 保留字段1
     */
    byte ACTION_RESERVE_01 = (byte) 0xA3;
    /***
     * 保留字段2
     */
    byte ACTION_RESERVE_02 = (byte) 0xA4;
    /***
     * 保留字段3
     */
    byte ACTION_RESERVE_03 = (byte) 0xA5;
    /***
     * 白板<->手机，除指令重发,应答及信息请求,连接请求,错误通告信息等自有应答或无需应答指令外,收到其它正常指令,要给出应答信号
     */
    byte ACTION_RESPONSE_RECEIVED = (byte) 0xA6;
    /***
     * 白板->手机, 请求白板发送现有的固件版本号等信息（无需手机发送正常应答0xA6）
     */
    byte ACTION_REQUEST_HARDWARE = (byte) 0xA7;
    /***
     * 请求当前录制状态信息
     */
    byte ACTION_REQUEST_CURRENT_RECORD_STATUS = (byte) 0xA8;
    /***
     * 手机心跳指令、
     */
    byte ACTION_BEAT = (byte) 0xA9;

    /* --------------------------------------------------------------- */
    // 带参数的消息类型
    /***
     * 提示手机抓屏,测试使用
     */
    int FUNC_REQUEST_SNAPSHOT_INFO = (byte) 0x70;
    /**
     * 参数为四个字节：unsigned int nms，指示手机录像带时间参数，由白板->手机
     */
    byte FUNC_REQUEST_START_RECORDING = (byte) 0x71;
    /**
     * 参数为四个字节：unsigned int nms，指示手机录像结束带时间参数，由白板->手机
     */
    byte FUNC_REQUEST_STOP_RECORDING = (byte) 0x72;
    /**
     * 参数为四个字节：unsigned int nms，手机向白板要求对时，由手机->白板 ，（无需白板发送正常应答0xA6）
     */
    byte FUNC_REQUEST_TIME_REVISION = (byte) 0x73;
    /**
     * 参数为四个字节：unsigned int nms，手机向白板要求对时，由手机->白板 ，（无需白板发送正常应答0xA6）
     */
    byte FUNC_RESPONSE_TIME_REVISION = (byte) 0x74;
    /**
     * 参数为四个字节：unsigned int nms，手机向白板要求对时，由手机->白板 ，（无需白板发送正常应答0xA6）
     */
    byte FUNC_REQUEST_CONNECTING = (byte) 0x75;
    /**
     * 连接应答成功
     */
    byte FUNC_RESPONSE_CONNECTING_SUCCESS = (byte) 0x76;
    /**
     * 连接应答错误
     */
    byte FUNC_RESPONSE_CONNECTING_ERROR = (byte) 0x77;
    /**
     * 错误信息返回
     */
    byte FUNC_RESPONSE_ERROR = (byte) 0x78;
    /**
     * 提示信息控制
     */
    byte FUNC_REQUEST_INFO_CONTROL = (byte) 0x79;
    /**
     * 返回当前的录制状态
     */
    byte FUNC_RESPONSE_CURRENT_RECORD_STATUS = (byte) 0xB0;
    /**
     * 返回硬件信息
     */
    byte FUNC_RESPONSE_HARDWARE = (byte) 0xB1;
    /**
     * 返回屏幕信息
     */
    byte FUNC_RESPONSE_SCREEN = (byte) 0xB2;
    /**
     * 设置二维码信息
     */
    byte FUNC_REQUEST_SET_QR_CODE = (byte) 0x5A;
    /**
     * 设置触屏信息
     */
    byte FUNC_REQUEST_SET_SCREEN = (byte) 0x5B;
    /**
     * 设置触屏点变化的有效阈值
     */
    byte FUNC_REQUEST_SET_THRESHOLD = (byte) 0x5C;
    /**
     * 设置点的发送时间间隔
     */
    byte FUNC_REQUEST_SET_INTERVAL = (byte) 0x5D;
    /**
     * 接收到点的数据
     */
    byte FUNC_POINT_DATA = (byte) 0xDD;

    /* ------------------------------------------------------------------ */

    /**
     * 消息头
     */
    byte[] MESSAGE_HEADER = {(byte) 0xFA, (byte) 0x0A};

    /**
     * 消息尾
     */
    byte[] MESSAGE_END = {(byte) 0xED};

    /**
     * 应答信息
     */
    byte RESPONSE_CODE = 0x77;

    /**
     * 所查询信息正常
     */
    byte[] RESPONSE_OK = {0, 0};
    /**
     * 已有用户连接
     */
    byte[] RESPONSE_USER_CONNECTED = {01, 75};
    /**
     * FLASH 上次非正常退出中时队列中有残留数据
     */
    byte[] RESPONSE_DATA_DISCARD = {(byte) 0xD1, 78};
    /**
     * 数据队列溢出
     */
    byte[] RESPONSE_DATA_OVERLOAD = {(byte) 0xE1, 78};
    /**
     * 触屏工作不正常
     */
    byte[] RESPONSE_TOUCH_SCREEN_INVALID = {(byte) 0xE2, 78};
    /**
     * 数据缓存不正常
     */
    byte[] RESPONSE_DATA_CACHE_INVALID = {(byte) 0xE4, 78};
    /**
     * 外部电压不足
     */
    byte[] RESPONSE_LOW_VOLTAGE = {(byte) 0xE8, 78};
    /**
     * 固件升级中
     */
    byte[] RESPONSE_FIRMWARE_UPGRADING = {(byte) 0xF3, 78};
    /**
     * 硬件有其它错误
     */
    byte[] RESPONSE_UNKNOWN_HARDWARE_ISSUE = {(byte) 0xFE, 78};
    /**
     * 白板录制处于非准备好状态
     */
    byte[] RESPONSE_BOARD_VIDEO_RECORD_NOT_READY = { 0x10, 79};
    /**
     * 白板录制处于准备录制状态
     */
    byte[] RESPONSE_BOARD_VIDEO_RECORD_READY = { 0x11, 79};
    /**
     * 白板录制处于正在录制状态
     */
    byte[] RESPONSE_BOARD_VIDEO_RECORDING = { 0x13, 79};

    // 定义消息各个部分的长度
    /**
     * 包头长度
     */
    int LENGTH_HEADER = 2;
    /**
     * 命令长度
     */
    int LENGTH_FEATURE = 1;
    /**
     * 目录 id长度
     */
    int LENGTH_ID = 2;
    /**
     * 数据长度
     */
    int LENGTH_DATA_SIZE = 2;
    /**
     * 校验位长度
     */
    int LENGTH_CHECKSUM = 1;
    /**
     * 包尾长度
     */
    int LENGTH_END = 1;
}
