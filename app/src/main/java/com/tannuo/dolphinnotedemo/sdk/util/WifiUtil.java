package com.tannuo.dolphinnotedemo.sdk.util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by hucn on 2016/7/29.
 * Description:
 */
public class WifiUtil {

    public static String convertIntIpAddress(int ipAdress) {
        return (ipAdress & 0xFF ) + "." +
                ((ipAdress >> 8 ) & 0xFF) + "." +
                ((ipAdress >> 16 ) & 0xFF) + "." +
                ( ipAdress >> 24 & 0xFF) ;
    }

    public static int byte2Int(byte[] b) {
        if (b.length == 1) {
            return b[0] & 0xff;
        }else if (b.length == 2) {
            return ((b[0] & 0xff) << 8) | (b[1] & 0xff);
        }else if (b.length == 3) {
            return ((b[0] & 0xff) << 16) | ((b[1] & 0xff) << 8) | (b[2] & 0xff);
        }else if (b.length >= 4){
            ByteBuffer converter = ByteBuffer.wrap(b);
            converter.order(ByteOrder.nativeOrder());
            return converter.getInt();
        }else {
            throw new RuntimeException("byte length > 4 or < 1");
        }
    }
}
