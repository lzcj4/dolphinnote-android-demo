package com.tannuo.dolphinnotedemo.sdk.device;

import java.io.File;

/**
 * Created by hucn on 2016/8/2.
 * Description: 存储点数据的接口
 */
public interface IPointsCache {

    void initialize();

    void putData(byte[] data);

    byte[] getCachedData();

    void clearCache();

    void clear();

    void remove();

    void snapshot();

    void cacheFromFile(String file);

    void setCacheSize(int size);

    void setCacheFile(File file);

}
