package com.tannuo.dolphinnotedemo.sdk.device;

/**
 * Created by hucn on 2016/8/2.
 * Description:
 */
public interface IService {

    int connect();

    void disconnect();

    void beatAlive();

    void write(byte[] bytes);

    void setOnReceiverListener(MessageReceiveListener receiveListener);
}
