package com.tannuo.dolphinnotedemo.sdk.device;

/**
 * Created by nick on 2016/4/23.
 */
public interface IDevice {
    // 连接
    int connect(String name);
    // 断开
    void disconnect();
    // 写入数据
    void write(byte[] data);
    // 截屏
    void capture();
    // 录制
    void record();
}
