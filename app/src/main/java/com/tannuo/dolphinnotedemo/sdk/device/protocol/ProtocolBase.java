package com.tannuo.dolphinnotedemo.sdk.device.protocol;

import com.tannuo.dolphinnotedemo.sdk.device.TouchPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nick_PC on 2016/7/1.
 */
public abstract class ProtocolBase implements IProtocol {
    protected final String TAG = this.getClass().getSimpleName();

    public static final int STATUS_CHANGE_DATA_FEATURE = 1;
    public static final int STATUS_GET_DATA = 2;
    public static final int STATUS_GET_GESTURE = 3;
    public static final int STATUS_GET_SNAPSHOT = 4;
    public static final int STATUS_GET_IDENTI = 5;
    public static final int STATUS_GET_SCREEN_FEATURE = 6;

    protected static final int ERROR_NONE = 0;
    protected static final int ERROR_HEADER = -1;
    protected static final int ERROR_DATA = -2;
    protected static final int ERROR_DATA_LENGTH = -3;
    protected static final int ERROR_DATA_FEATURE = -4;
    protected static final int ERROR_CHECKSUM = -5;
    // ======= Created by hucn: add the error type =========
    protected static final int ERROR_ID = -6;
    protected static final int ERROR_END = -7;


    // ======= Created by hucn: add the feature type =========

    public static final int STATUS_REQUEST_SNAPSHOT = 4; // 由白板->手机，提示手机抓屏

    public static final int STATUS_REQUEST_TO_DISCONNECT = 7; // 白板<->手机，手机和白板连接相互结束，白板处待连状态

    public static final int STATUS_REQUEST_TO_REPEAT = 8; // 白板<->手机，请求对方再次发送上次的数据包

    public static final int STATUS_RESPONSE_RECEIVED = 9; // 白板<->手机，除指令重发,应答及信息请求,连接请求,错误通告信息等自有应答或无需应答指令外,收到其它正常指令,要给出应答信号

    public static final int STATUS_REQUEST_HARDWARE = 10; // 白板->手机, 请求白板发送现有的固件版本号等信息（无需手机发送正常应答0xA6）

    public static final int STATUS_REQUEST_CURRENT_RECORD_STATUS = 11; // 请求当前录制状态信息

    public static final int STATUS_BEAT = 12; // 手机心跳指令

    public static final int STATUS_REQUEST_SNAPSHOT_INFO = 13; // 拍照带时, 与STATUS_REQUEST_SNAPSHOT功能相同，带有时间参数，测试时使用

    public static final int STATUS_REQUEST_START_RECORDING = 14; // 参数为四个字节：unsigned int nms，指示手机录像带时间参数，由白板->手机

    public static final int STATUS_REQUEST_STOP_RECORDING = 15; // 参数为四个字节：unsigned int nms，指示手机录像结束带时间参数，由白板->手机

    public static final int STATUS_REQUEST_TIME_REVISION = 16; // 参数为四个字节：unsigned int nms，手机向白板要求对时，由手机->白板 ，

    public static final int STATUS_RESPONSE_TIME_REVISION = 17; // 参数为四个字节：unsigned int nms，手机向白板要求对时，由手机->白板

    public static final int STATUS_REQUEST_CONNECTING = 18; // 参数为四个字节：unsigned int nms，手机向白板要求对时，由手机->白板

    public static final int STATUS_RESPONSE_CONNECTING_SUCCESS = 19; // 连接应答成功

    public static final int STATUS_RESPONSE_CONNECTING_ERROR = 20; // 连接应答错误

    public static final int STATUS_RESPONSE_ERROR = 21; // 错误信息返回

    public static final int STATUS_REQUEST_INFO_CONTROL = 22; // 提示信息控制

    public static final int STATUS_RESPONSE_CURRENT_RECORD_STATUS = 23; // 返回当前的录制状态

    public static final int STATUS_RESPONSE_HARDWARE = 24; // 返回硬件信息

    public static final int STATUS_RESPONSE_SCREEN = 25; // 返回屏幕信息

    public static final int STATUS_REQUEST_SET_QR_CODE = 26; // 设置二维码信息

    public static final int STATUS_REQUEST_SET_SCREEN = 27; // 设置触屏信息

    public static final int STATUS_REQUEST_SET_THRESHOLD = 28; // 设置触屏点变化的有效阈值

    public static final int STATUS_REQUEST_SET_INTERVAL = 29; // 设置点的发送时间间隔

    public static final int STATUS_POINT_DATA = 2; // 接收到点的数据，与STATUS_GET_DATA相同

    public static final int STATUS_POINT_DATA_FROM_REMOTE = 100; // 从远程服务端发送过来的数据

    // ======= Created by hucn =========

    public List<TouchPoint> mPoints = new ArrayList<>();

    protected byte[] lastUnhandledBytes = new byte[0];

    protected byte[] combineBytes(byte[] data1, byte[] data2) {
        byte[] result = new byte[0];
        if (null == data1 && null == data2) {
            return result;
        }
        if (null == data2) {
            result = data1;
        }

        if (null == data1) {
            result = data2;
        }

        if (null != data1 && null != data2) {
            result = new byte[data1.length + data2.length];
            System.arraycopy(data1, 0, result, 0, data1.length);
            System.arraycopy(data2, 0, result, data1.length, data2.length);
        }

        return result;
    }

    protected void reset() {
        mPoints.clear();
    }
}
