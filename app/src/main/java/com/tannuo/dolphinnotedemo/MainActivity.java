package com.tannuo.dolphinnotedemo;

import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;

import com.tannuo.dolphinnotedemo.sdk.device.ConnectionListener;
import com.tannuo.dolphinnotedemo.sdk.device.DeviceScreen;
import com.tannuo.dolphinnotedemo.sdk.device.error.ConnectionErrorCode;
import com.tannuo.dolphinnotedemo.sdk.device.protocol.IProtocol;
import com.tannuo.dolphinnotedemo.sdk.device.protocol.ZDMessage;
import com.tannuo.dolphinnotedemo.sdk.device.protocol.ZDProtocol;
import com.tannuo.dolphinnotedemo.sdk.device.wifi.WifiService;
import com.tannuo.dolphinnotedemo.sdk.util.DataUtil;
import com.tannuo.dolphinnotedemo.sdk.util.Logger;

import java.nio.ByteBuffer;


public class MainActivity extends AppCompatActivity {
    String TAG = "MainActivity";
    ByteBuffer buffer = ByteBuffer.allocate(12);
    int idFlag = 0;
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        service.connect();
    }

    IProtocol protocol = new ZDProtocol();
    DeviceScreen screen = new DeviceScreen();
    WifiService service = new WifiService(this, protocol, screen, new ConnectionListener() {
        @Override
        public void onError(ConnectionErrorCode errorCode) {
            Log.e(TAG, "errorCode：" + errorCode.Msg);
        }

        @Override
        public void onConnectFail() {
            Log.e(TAG, "onConnectFail");
        }

        @Override
        public void onConnected() {

        }

        @Override
        public void onDisconnected() {

        }

        @Override
        public void onMultiSuccess() {
            Log.e(TAG, "onMultiSuccess");
        }

        @Override
        public void onMultiFail() {
        }
    });

//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        int action = MotionEventCompat.getActionMasked(event);
//        switch (action) {
//            case (MotionEvent.ACTION_DOWN):
//            case (MotionEvent.ACTION_MOVE):
//                short x = (short) event.getX();
//                short y = (short) event.getY();
//                long timeMillis = (System.currentTimeMillis());
//                sendData(x, y, idFlag, timeMillis);
//                return false;
//            case (MotionEvent.ACTION_UP):
//                sendData((short) event.getX(), (short) event.getY(), idFlag | (0x80), (System.currentTimeMillis()));
//                if (idFlag > 127) {
//                    idFlag = 0;
//                } else {
//                    idFlag++;
//                }
//                return false;
//            default:
//                return super.onTouchEvent(event);
//        }
//    }

    public void setPoints(MotionEvent event) {
        int action = MotionEventCompat.getActionMasked(event);
        switch (action) {
            case (MotionEvent.ACTION_DOWN):
            case (MotionEvent.ACTION_MOVE):
                short x = (short) event.getX();
                short y = (short) event.getY();
                long timeMillis = (System.currentTimeMillis());
                sendData(x, y, idFlag, timeMillis);
                break;
            case (MotionEvent.ACTION_UP):
                sendData((short) event.getX(), (short) event.getY(), idFlag | (0x80), (System.currentTimeMillis()));
                if (idFlag > 127) {
                    idFlag = 0;
                } else {
                    idFlag++;
                }
                //return false;
            default:
                //return super.onTouchEvent(event);
        }
    }

    int xTmp = 0, yTmp = 0;

    public void sendData(short x, short y, int id, long time) {
        buffer.put(DataUtil.int2byteLen2Low(x), 0, 2);
        buffer.put(DataUtil.int2byteLen2Low(y), 0, 2);
        buffer.put(DataUtil.int2byteLen2Low(1), 0, 2);
        buffer.put(DataUtil.int2byteLen2Low(1), 0, 2);
        buffer.put(new byte[]{(byte) id}, 0, 1);//id
        byte[] times = DataUtil.int2byteLittleEndian((int) time);
        buffer.put(new byte[]{times[0], times[1], times[2]}, 0, 3);
        count++;
        Logger.e("udp:", "x= " + x + " y= " + y + " id =" + id);
        Logger.e("udp:", "time=" + time);
        Logger.e("udp:", "time=" + Long.valueOf(time).intValue());
        Logger.e("udp:", "time=" + DataUtil.bytesToIntLittleEndian(new byte[]{(byte) times[0], (byte) times[1], (byte) times[2], (byte) times[3]}) + "");
        if (count % 1 == 0) {
            Logger.e("udp:", "send...... count=" + count);
            ZDMessage msgBase = new ZDMessage();
            msgBase.setId(new byte[]{0x11, 0x11});
            msgBase.setFeature(new byte[]{(byte) (0xdd)});
            msgBase.setData(buffer.array());
            service.write(msgBase.getBytes());
            buffer = ByteBuffer.allocate(12);
            count = 0;
        }
    }
}
